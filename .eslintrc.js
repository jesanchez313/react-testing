module.exports = {
  env: {
    'jest/globals': true,
  },
  extends: ['airbnb', 'airbnb/hooks', 'plugin:jest/recommended'],
  plugins: ['jest'],
};
