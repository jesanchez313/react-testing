import React, { useState } from 'react';
import { min, sum } from './utils/calc';

function App() {
  const [num1, setNum1] = useState('');
  const [num2, setNum2] = useState('');
  const [result, setResult] = useState(0);

  const cal = (options) => {
    switch (options.operation) {
      case 'SUM':
        setResult(sum(Number(num1), Number(num2)));
        break;
      case 'MIN':
        setResult(min(Number(num1), Number(num2)));
        break;

      default:
        break;
    }
  };

  return (
    <div className="flex justify-center bg-gray-300 h-screen items-center">
      <div className="bg-white p-10 rounded">
        <h2
          id="title"
          className="w-full flex justify-center mb-5 font-semibold uppercase"
        >
          The best calculator
        </h2>
        <form>
          <div>
            <input
              className="border-gray-300 border-1 border rounded h-14 px-5"
              type="text"
              data-testid="number1"
              id="number1"
              value={num1}
              onChange={(event) => setNum1(event.target.value)}
              placeholder="Put number 1"
            />
          </div>
          <div>
            <input
              className="border-gray-300 border-1 border rounded h-14 px-5"
              type="text"
              id="number2"
              data-testid="number2"
              value={num2}
              onChange={(event) => setNum2(event.target.value)}
              placeholder="Put number 2"
            />
          </div>
        </form>
        <div className="mt-5">
          Resultado:
          <span data-testid="result">{result}</span>
        </div>
        <div className="flex gap-3 mt-5">
          <button
            onClick={() => cal({ operation: 'SUM' })}
            type="button"
            data-testid="buttonSum"
            className="bg-blue-600 text-white px-5 py-5 rounded w-full"
          >
            SUM
          </button>
          <button
            onClick={() => cal({ operation: 'MIN' })}
            type="button"
            data-testid="buttonMin"
            className="bg-blue-600 text-white px-5 py-5 rounded w-full"
          >
            MINUS
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
