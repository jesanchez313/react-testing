export function sum(num1, num2) {
  if (typeof num1 !== 'number' || typeof num2 !== 'number') {
    return "Any of params isn't a number";
  }

  return num1 + num2;
}

export function min(num1, num2) {
  return num1 - num2;
}
