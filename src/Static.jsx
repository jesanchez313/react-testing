import logo from './logo.svg';
import './App.css';

function Static() {
  return (
    <div className="App">
      <header className="App-header">Static test</header>
    </div>
  );
}

export default App;
