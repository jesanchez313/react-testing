import { sum } from './utils/calc';

describe('Test calculator', () => {
  describe('Unit test', () => {
    it('Unit test - function sum', () => {
      const result = sum(Number(3), Number(5));

      expect(result).toEqual(8);
    });

    it('Unit test - function sum with param string', () => {
      const result = sum(Number(3), 'test');

      expect(result).toEqual("Any of params isn't a number");
    });
  });
});
